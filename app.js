// Задание
// Дополнить функцию createNewUser() методами подсчета возраста 
// пользователя и его паролем. Задача должна быть реализована на 
//языке javascript, без использования фреймворков и сторонник 
// библиотек (типа Jquery).

// Технические требования:

// Возьмите выполненное домашнее задание номер 4 (созданная вами 
// функция createNewUser()) и дополните ее следующим функционалом:

// При вызове функция должна спросить у вызывающего дату рождения 
// (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
// Создать метод getAge() который будет возвращать сколько пользователю лет.
// Создать метод getPassword(), который будет возвращать первую букву имени 
// пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) 
// и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).


// Вывести в консоль результат работы функции createNewUser(), а также функций 
// getAge() и getPassword() созданного объекта.

function createNewUser (){

    const firstName = prompt ('Your name: ')
    const lastName = prompt ('Your surname:')
    let userBirth;
    do {  
    userBirth=prompt ('Your birth date:', 'dd.mm.yyyy')
    } while (!/\d{2}.\d{2}.\d{4}/.test(userBirth))

    return {
        firstName,
        lastName,
        userBirth,
        getAge (){
            let currentYear = new Date().getFullYear()
            let currentMonth = new Date().getMonth()
            let currentDay = new Date().getDate()
            const birthDay = userBirth.slice(0,2)
            const birthMonth = userBirth.slice(3,5)
            const birthYear = userBirth.slice(-4)
            let age = currentYear - birthYear
            if (birthMonth > currentMonth || (birthMonth === currentMonth && birthDay > currentDay)){
                age--
            }
            
            return age
        },
        getPassword (){
            return(this.firstName[0]+(this.lastName.toLowerCase())+this.userBirth.slice(-4))
        }
    }
}

let user= createNewUser()
console.log(user.firstName+' '+user.lastName)
console.log(user.getAge())
console.log(user.getPassword())
